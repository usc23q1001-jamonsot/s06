from abc import ABC, abstractmethod

class Person(ABC):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        
    def getFullName(self):
        return f"{self._firstName} {self._lastName}"
    
    @abstractmethod
    def login(self):
        pass
    
    @abstractmethod
    def logout(self):
        pass
    
    # You can define addRequest and checkRequest methods here, since they are the same for all subclasses
    def addRequest(self):
        return "Request has been added"
    
    def checkRequest(self):
        return "Checked request"
    
class Employee(Person):
    def login(self):
        return f"{self._email} has logged in"
    
    def logout(self):
        return f"{self._email} has logged out"
    
class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__(firstName, lastName, email, department)
        self._members = []
        
    def login(self):
        return f"{self._email} has logged in"
    
    def logout(self):
        return f"{self._email} has logged out"
    
    def addMember(self, member):
        self._members.append(member)
        
    def get_members(self):
        return self._members
    
class Admin(Person):
    def login(self):
        return f"{self._email} has logged in"
    
    def logout(self):
        return f"{self._email} has logged out"
    
    def addUser(self):
        return "User has been added"
    
class Request:
    def __init__(self, name, requester, dateRequested):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = "open"
        
    def updateRequest(self, newRequest):
        self._name = newRequest
    
    def closeRequest(self):
        self._status = "closed"
        return f"{self._name} has been closed"
    
    def cancelRequest(self):
        self._status = "cancelled"
        return f"{self._name} has been cancelled"
    
    def set_status(self, status):
        self._status = status
        
    def get_status(self):
        return self._status

# Create instances of the classes
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

# Test some methods and assertions
assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())